package com.logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

/*
        error   you need        to do something
        warn    you might need  to do something
        info    you need        to log this in production
        debug   you might need  to log this in production
        trace   everything that is happening (no performance concerns)
*/

@RestController
public class controller {
    Logger logger = LoggerFactory.getLogger(controller.class);
    Random rand = new Random();

    @GetMapping("/bool")
    public Boolean returntruefalse(){
        logger.info("Generating Boolean value running");
        return rand.nextInt() % 2 == 0;
    }
    @GetMapping("/nodef")
    public void error(){
        logger.error("No defination");
    }
    /*
    @GetMapping("/trace")
    public Boolean trace(){
        logger.trace("Trace");
        return returntruefalse();

    }*/
}
